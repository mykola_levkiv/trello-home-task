function findWinner(votes) {
  validateVotes(votes);

  const votingResults = getVotingResults(votes);
  const winner = getVotingWinner(votingResults);

  return {
    votes: votingResults,
    winner,
  };
}

function validateVotes(votes) {
  const areVotesValid =
    votes && votes.length && votes.every((vote) => typeof vote === "string");

  if (!areVotesValid) {
    throw new Error("Votes should be valid");
  }
}

function getVotingResults(votes) {
  return votes.reduce((countPerVote, voteOption) => {
    const votesCount = countPerVote[voteOption] || 0;
    countPerVote[voteOption] = votesCount + 1;

    return countPerVote;
  }, {});
}

function getVotingWinner(votingResults) {
  const winnerStats = Object.entries(votingResults).reduce(
    (winner, [vote, count]) => {
      if (count > winner.count) {
        return {
          vote: vote,
          count: count,
        };
      }

      return winner;
    },
    {
      vote: null,
      count: 0,
    }
  );

  return winnerStats.vote;
}
