const VOTES = ["A", "B", "A", "C", "D", "B", "A"];

const RESULT_CONTAINER_CLASS = "voting-results-container";
const WINNER_BTN_CLASS = "winner-btn";

const findWinnerBtn = document.querySelector(`.${WINNER_BTN_CLASS}`);
if (!findWinner) {
  throw new Error("Find winner button not found");
}

findWinnerBtn.onclick = function () {
  const votingResults = findWinner(VOTES);

  removeExistingContainer();

  const resultsContainer = new ResultsContainerBuilder()
    .addDescription("List of Votes:")
    .addVotesList(votingResults.votes)
    .addWinner(votingResults.winner)
    .get();

  document.body.appendChild(resultsContainer);
};

function removeExistingContainer() {
  const existingResultElement = document.querySelector(
    `.${RESULT_CONTAINER_CLASS}`
  );
  if (existingResultElement) {
    existingResultElement.remove();
  }
}

class ResultsContainerBuilder {
  constructor() {
    const resultsContainer = document.createElement("div");
    resultsContainer.classList.add(RESULT_CONTAINER_CLASS);

    this.resultsContainer = resultsContainer;
  }

  addDescription(description) {
    const descriptionElement = document.createElement("p");
    descriptionElement.innerText = description;

    this.resultsContainer.appendChild(descriptionElement);

    return this;
  }

  addVotesList(votes) {
    const votesList = document.createElement("ul");
    Object.entries(votes).forEach(([vote, count]) => {
      const voteElement = document.createElement("li");
      voteElement.innerText = `${vote}: ${count}`;
      votesList.appendChild(voteElement);
    });

    this.resultsContainer.appendChild(votesList);

    return this;
  }

  addWinner(winner) {
    const winnerElement = document.createElement("p");
    winnerElement.innerText = `The winner is ${winner}!`;
    this.resultsContainer.appendChild(winnerElement);

    return this;
  }

  get() {
    return this.resultsContainer;
  }
}
